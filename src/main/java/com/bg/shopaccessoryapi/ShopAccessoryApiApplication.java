package com.bg.shopaccessoryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopAccessoryApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopAccessoryApiApplication.class, args);
    }

}
