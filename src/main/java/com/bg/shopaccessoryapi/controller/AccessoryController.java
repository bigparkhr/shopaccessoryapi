package com.bg.shopaccessoryapi.controller;

import com.bg.shopaccessoryapi.model.*;
import com.bg.shopaccessoryapi.service.AccessoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.lang.constant.Constable;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/accessory")
public class AccessoryController {
    private final AccessoryService accessoryService;

    @PostMapping("/shop")
    public CommonResult setAccessory(@RequestBody AccessoryRequest request) {
        accessoryService.setAccessory(request);

        ListResult<AccessoryItem> response = new ListResult<>();
        response.setCode(0);
        response.setMsg("성공");

        return response;
    }
    @GetMapping("/all")
    public ListResult<AccessoryItem> getAccessorys() {
        List<AccessoryItem> List = accessoryService.getAccessorys();

        ListResult<AccessoryItem> response = new ListResult<>();
        response.setList(List);
        response.setTotalCount(List.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");
        return response;
    }
    @GetMapping("/detail/{id}")
    public SingleResult<AccessoryResponse> getDetail(@PathVariable long id) {
        AccessoryResponse result = accessoryService.getAccessory(id);

        SingleResult<AccessoryResponse> response = new SingleResult<>();
        response.setMsg("성공");
        response.setCode(0);
        response.setData(result);

        return response;
    }
}
