package com.bg.shopaccessoryapi.repository;

import com.bg.shopaccessoryapi.entity.Accessory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccessoryRepository extends JpaRepository<Accessory,Long> {
}
