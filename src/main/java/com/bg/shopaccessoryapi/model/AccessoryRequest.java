package com.bg.shopaccessoryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessoryRequest {
    private String imageName;
    private String name;
    private Double price;
}
