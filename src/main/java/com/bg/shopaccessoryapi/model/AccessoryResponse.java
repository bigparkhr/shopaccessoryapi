package com.bg.shopaccessoryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessoryResponse {
    private Long id;
    private String imageName;
    private String name;
    private Double price;
    private String etcMemo;
}
