package com.bg.shopaccessoryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessoryItem {
    private Long id;
    private String imageName;
    private String name;
    private Double price;
}
