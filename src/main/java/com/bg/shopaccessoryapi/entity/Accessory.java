package com.bg.shopaccessoryapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Accessory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, length = 30)
    private String imageName;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Double price;
}
