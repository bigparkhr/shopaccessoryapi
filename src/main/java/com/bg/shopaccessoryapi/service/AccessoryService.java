package com.bg.shopaccessoryapi.service;

import com.bg.shopaccessoryapi.entity.Accessory;
import com.bg.shopaccessoryapi.model.AccessoryItem;
import com.bg.shopaccessoryapi.model.AccessoryRequest;
import com.bg.shopaccessoryapi.model.AccessoryResponse;
import com.bg.shopaccessoryapi.repository.AccessoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccessoryService {
    private final AccessoryRepository accessoryRepository;

    public void setAccessory(AccessoryRequest request) {
        Accessory addData = new Accessory();
        addData.setImageName(request.getImageName());
        addData.setName(request.getName());
        addData.setPrice(request.getPrice());

        accessoryRepository.save(addData);
    }

    public List<AccessoryItem> getAccessorys() {
        List<Accessory> originList = accessoryRepository.findAll();

        List<AccessoryItem> result = new LinkedList<>();

        for (Accessory accessory : originList) {
            AccessoryItem addItem = new AccessoryItem();
            addItem.setId(accessory.getId());
            addItem.setImageName(accessory.getImageName());
            addItem.setName(accessory.getName());
            addItem.setPrice(accessory.getPrice());

            result.add(addItem);
        }
        return result;
    }

    public AccessoryResponse getAccessory(long id) {
        Accessory accessory = accessoryRepository.findById(id).orElseThrow();

        AccessoryResponse response = new AccessoryResponse();
        response.setId(accessory.getId());
        response.setImageName(accessory.getImageName());
        response.setName(accessory.getName());
        response.setPrice(accessory.getPrice());

        return response;
    }
}
